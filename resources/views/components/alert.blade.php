<div class="alert alert-{{$type}} text-center" role="alert">
    <i class="fa-solid fa-{{$iconContent}}"></i> {{$message}}
</div>