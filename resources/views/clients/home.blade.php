@extends('layouts.client')


@section('sidebar')
    {{-- @parent --}}
    <h3>Home sidebar</h3>
@endsection

@section('content')
    {{-- <h1>Trang chủ</h1>
    <button type="button" class="show">show</button> --}}
    <section>
        <div class="container">
            <h1>Trang chủ</h1>
        </div>
    </section>
    <x-package-alert type='{{$type}}' iconContent='{{$icon}}' message='{{$mess}}'/>
    <x-button/>
@endsection


@push('scripts')
    <script>
        console.log('Helo!');
    </script>
@endpush


{{-- @section('css')
    header{
        color: blue
    }
@endsection --}}

{{-- @section('js')
    document.querySelector('.show').onclick = function(){
        alert('Thành công');
    }
@endsection --}}
