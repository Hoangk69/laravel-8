<h1>Học views Engine</h1>

{{-- hiển thị biến --}}
<h2>{{$age}}</h2>

{{-- hiển thị biến với đoạn mã html --}}
<p>{!!$titlePapaer!!}</p>

{{-- vong lap for --}}
@for ($i = 0; $i < 10; $i++)
    <p> phần tử thứ {{$i}} </p>
@endfor

<hr>
{{-- vong lap while --}}
@while ($index<10)
    <p>phần tử thứ {{$index}}</p>
    @php
        $index++;
    @endphp
@endwhile

<hr>

{{-- vong lap foreach --}}
@foreach ($arr as $key => $item)
    <p>duyệt từng item {{$item}}-{{$key}}</p>
@endforeach

<hr>

{{-- vong lap forelse--}}
@forelse ($arr as $item)
    <p>duyệt từng item {{$item}}</p>
@empty
    <p>Không có phần tử nào trong mảng</p>
@endforelse

<hr>

{{-- if else --}}
@if ($check)
    <p>True</p>
@else
    <p>false</p>
@endif

<hr>

{{-- raw php --}}
@php
    $number = 10;
    $total = $number + 20;
@endphp
<p>Total : {{ $total }}</p>

{{-- include sub view --}}
@php
    $include_var = 'get title include sub view';
@endphp
@include('HocReq.getForm');
