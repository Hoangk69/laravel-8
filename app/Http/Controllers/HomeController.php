<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public $data = [];
    //
    public function index()
    {
        // /home
        $this->data['type'] = 'danger';
        $this->data['mess'] = 'error';
        $this->data['icon'] = 'circle-exclamation';
        return view('clients.home', $this->data);
    }

    public function getForm()
    {
        // /them-san-pham
        return view('clients.add');
    }

    public function postForm(Request $req)
    {
        // /them-san-pham
        dd($req);
    }

    public function putForm(Request $req)
    {
        // /them-san-pham
        echo 'this is a method PUT';
        dd($req);
    }
}
