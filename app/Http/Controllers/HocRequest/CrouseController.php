<?php

namespace App\Http\Controllers\HocRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CrouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        //
        //$allData = $req->all();
        $allData = $req->path();
        //echo $allData['id'];
        //dd($allData);

        /* The is method allows you to verify that the
        incoming request path matches a given pattern.
        */
        echo 'Check path: ';
        if ($req->is('hoc-request')) {
            echo 'true <br>';
        }else{
            echo 'false <br>';
        }

        $url = $req->url();
        echo $url;

        echo '<br>';
        $fullUrl = $req->fullUrl();
        echo $fullUrl;

        echo '<br>';
        $method = $req->method();
        echo $method;

        //
        echo '<br>';
        echo $req->ip();

        // data req
        $input = $req->input();
        dd($input);
        return view('HocReq.hocReq');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('HocReq.getForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        //dd($req->all());
        return 'creat sucessfuly';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
