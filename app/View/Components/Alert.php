<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Alert extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $type, $message, $iconContent;

    public function __construct($type, $message, $iconContent)
    {
        //
        $this->type=$type;
        $this->message=$message;
        $this->iconContent=$iconContent;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.alert');
    }
}
