<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\HocRequest\CrouseController;
use App\Http\Controllers\HocViewController;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home.welcome');

Route::get('/form', function(){
    return view('form');
})->name('form.login');

Route::post('/form', function(){
    return "Method Post Resovle";
});

Route::prefix('admin')->middleware('checkpermission')->group(function (){
    Route::get('/product', function(){
        return "admin/product";
    });
    Route::get('/news', function(){
        return "admin/product";
    });
});

Route::get('/profile/{id}/{slug?}', function($id, $slug = null){
    return "the route have param: " . $id . " and slug: " .$slug;
})->name('detail.product');


// use controller

Route::get('/get-list-categories/{id}', [CategoriesController::class, 'getDetailCategory']);

Route::resource('product', ProductController::class);

Route::resource('hoc-request', CrouseController::class);

// hoc view engine blade
Route::resource('hoc-view', HocViewController::class);

// master layout
Route::get('home', [HomeController::class, 'index'])->name('home');

// get form blade
Route::get('/them-san-pham', [HomeController::class, 'getForm'])->name('product.add');

//Route::post('/them-san-pham', [HomeController::class, 'postForm']);

Route::put('/them-san-pham', [HomeController::class, 'putForm']);

